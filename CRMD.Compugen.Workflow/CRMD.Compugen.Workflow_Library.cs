﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Diagnostics;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;

namespace CRMD.Compugen.Library
{
    public class GetRunningWorkflowUser : CodeActivity
    {
        [Output("Running User")]
        [ReferenceTarget("systemuser")]
        public OutArgument<EntityReference> RunningUserReference { get; set; }

        [Output("LCID")]
        public OutArgument<Int32> LCIDReference { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            SetupOutArguments(executionContext);

            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            //Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            //Guid runningUserId = context.InitiatingUserId;
            Guid runningUserId = context.UserId;

            tracingService.Trace("Initiating User: " + context.InitiatingUserId.ToString());
            tracingService.Trace("User: " + context.UserId.ToString());

            Entity runningUser = new Entity("systemuser");
            runningUser = service.Retrieve("systemuser", runningUserId, new ColumnSet(true));

            this.RunningUserReference.Set(executionContext, runningUser.ToEntityReference());
            this.LCIDReference.Set(executionContext, GetUserLCID(service, runningUserId));
        }

        private void SetupOutArguments(CodeActivityContext context)
        {
            RunningUserReference.Set(context, null);

            LCIDReference.Set(context, 0);
        }

        public static Int32 GetUserLCID(IOrganizationService service, Guid userId)
        {
            QueryExpression userQuery = new QueryExpression
            {
                EntityName = "usersettings",
                ColumnSet = new ColumnSet("uilanguageid"),
                Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "systemuserid",
                                Operator = ConditionOperator.Equal,
                                Values = { userId }

                            }
                        }
                }
            };

            EntityCollection settings = service.RetrieveMultiple(userQuery);
            if (settings.Entities.Count > 0)
            {
                Int32 retInt;
                bool retVal = int.TryParse(settings.Entities[0].Attributes["uilanguageid"].ToString(), out retInt);
                if (retVal)
                {
                    return retInt;
                }


            }
            return 1033;
        }

        public string DoesUserBelongToRole(IOrganizationService service, string roleName, ref bool DoesUserHasRole, Guid userId)
        {
            string result = userId.ToString();
            DoesUserHasRole = false;

            if (userId != Guid.Empty)
            {
                try
                {
                    QueryExpression query = new QueryExpression
                    {
                        EntityName = "role",
                        ColumnSet = new ColumnSet("roleid"),
                        Criteria = new FilterExpression
                        {
                            Conditions =
                            {
                                new ConditionExpression
                                {
                                    AttributeName = "name",
                                    Operator = ConditionOperator.Equal,
                                    Values = {roleName}
                                }
                            }
                        }
                    };

                    // Get the role.
                    EntityCollection givenRoles = service.RetrieveMultiple(query);

                    if (givenRoles.Entities.Count > 0)
                    {
                        Entity oRole = givenRoles.Entities[0];

                        // Establish a SystemUser link for a query.
                        LinkEntity systemUserLink = new LinkEntity()
                        {
                            LinkFromEntityName = "systemuserroles",
                            LinkFromAttributeName = "systemuserid",
                            LinkToEntityName = "systemuser",
                            LinkToAttributeName = "systemuserid",
                            LinkCriteria =
                            {
                                Conditions =
                                {
                                    new ConditionExpression("systemuserid", ConditionOperator.Equal, userId)
                                }
                            }
                        };

                        // Build the query.
                        QueryExpression linkQuery = new QueryExpression()
                        {
                            EntityName = "role",
                            ColumnSet = new ColumnSet("roleid"),
                            LinkEntities =
                            {
                                new LinkEntity()
                                {
                                    LinkFromEntityName = "role",
                                    LinkFromAttributeName = "roleid",
                                    LinkToEntityName = "systemuserroles",
                                    LinkToAttributeName = "roleid",
                                    LinkEntities = {systemUserLink}
                                }
                            },
                            Criteria =
                            {
                                Conditions =
                                {
                                    new ConditionExpression("roleid", ConditionOperator.Equal, oRole.Id)
                                }
                            }
                        };

                        // Retrieve matching roles.
                        EntityCollection matchEntities = service.RetrieveMultiple(linkQuery);

                        // if an entity is returned then the user is a member of the role
                        Boolean isUserInRole = (matchEntities.Entities.Count > 0);

                        if (isUserInRole)
                            DoesUserHasRole = true;
                        else
                            DoesUserHasRole = false;

                    }
                }
                catch (Exception ex)
                {
                    result = "Error in DoesUserBelongToRole: " + ex.ToString();
                }
            }

            return result;
        }
    }

    public class GetUserHasRole : CodeActivity
    {
        [Input("Security Role Name")]
        public InArgument<String> RoleName { get; set; }

        [Output("User Has Role")]
        public OutArgument<bool> UserHasRole { get; set; }

        [Output("Error Message")]
        public OutArgument<string> ErrorMessage { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            SetupOutArguments(executionContext);

            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            //Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            //Guid runningUserId = context.InitiatingUserId;
            Guid runningUserId = context.UserId;

            tracingService.Trace("Initiating User: " + context.InitiatingUserId.ToString());
            tracingService.Trace("User: " + context.UserId.ToString());

            bool DoesUserHasRole = false;

            string CheckUserHasRoleResult = DoesUserBelongToRole(service, this.RoleName.Get(executionContext), ref DoesUserHasRole, runningUserId);

            if (CheckUserHasRoleResult.Contains("Error"))
            {
                this.UserHasRole.Set(executionContext, false);
                this.ErrorMessage.Set(executionContext, CheckUserHasRoleResult);
            }
            else
            {
                if (DoesUserHasRole)
                {
                    this.UserHasRole.Set(executionContext, true);
                    this.ErrorMessage.Set(executionContext, CheckUserHasRoleResult);
                }
                else
                {
                    this.UserHasRole.Set(executionContext, false);
                    this.ErrorMessage.Set(executionContext, CheckUserHasRoleResult);
                }
            }
        }

        private void SetupOutArguments(CodeActivityContext context)
        {
            UserHasRole.Set(context, false);
            ErrorMessage.Set(context, string.Empty);
        }

        public string DoesUserBelongToRole(IOrganizationService service, string roleName, ref bool DoesUserHasRole, Guid userId)
        {
            string result = userId.ToString();

            if (userId != Guid.Empty)
            {
                try
                {
                    QueryExpression query = new QueryExpression
                    {
                        EntityName = "role",
                        ColumnSet = new ColumnSet("roleid"),
                        Criteria = new FilterExpression
                        {
                            Conditions =
                            {
                                new ConditionExpression
                                {
                                    AttributeName = "name",
                                    Operator = ConditionOperator.Equal,
                                    Values = {roleName}
                                }
                            }
                        }
                    };

                    // Get the role.
                    EntityCollection givenRoles = service.RetrieveMultiple(query);

                    if (givenRoles.Entities.Count > 0)
                    {
                        Entity oRole = givenRoles.Entities[0];

                        // Establish a SystemUser link for a query.
                        LinkEntity systemUserLink = new LinkEntity()
                        {
                            LinkFromEntityName = "systemuserroles",
                            LinkFromAttributeName = "systemuserid",
                            LinkToEntityName = "systemuser",
                            LinkToAttributeName = "systemuserid",
                            LinkCriteria =
                            {
                                Conditions =
                                {
                                    new ConditionExpression("systemuserid", ConditionOperator.Equal, userId)
                                }
                            }
                        };

                        // Build the query.
                        QueryExpression linkQuery = new QueryExpression()
                        {
                            EntityName = "role",
                            ColumnSet = new ColumnSet("roleid"),
                            LinkEntities =
                            {
                                new LinkEntity()
                                {
                                    LinkFromEntityName = "role",
                                    LinkFromAttributeName = "roleid",
                                    LinkToEntityName = "systemuserroles",
                                    LinkToAttributeName = "roleid",
                                    LinkEntities = {systemUserLink}
                                }
                            },
                            Criteria =
                            {
                                Conditions =
                                {
                                    new ConditionExpression("roleid", ConditionOperator.Equal, oRole.Id)
                                }
                            }
                        };

                        // Retrieve matching roles.
                        EntityCollection matchEntities = service.RetrieveMultiple(linkQuery);

                        // if an entity is returned then the user is a member of the role
                        if (matchEntities.Entities.Count > 0)
                        {
                            DoesUserHasRole = true;
                            //result = "Error: " + matchEntities.Entities.Count.ToString();
                        }
                        else
                        {
                            DoesUserHasRole = false;
                            //result = "Error: " + matchEntities.Entities.Count.ToString();
                        }

                    }
                }
                catch (Exception ex)
                {
                    result = "Error in DoesUserBelongToRole: " + ex.ToString();
                }
            }

            return result;
        }
    }

    public class CGNEmailRouting : CodeActivity
    {
        [Output("Error Message")]
        public OutArgument<string> ErrorMessage { get; set; }

        [Output("Request Owner User")]
        [ReferenceTarget("systemuser")]
        public InOutArgument<EntityReference> RequestOwnerUser { get; set; }

        [Input("Default Team Owner")]
        [Output("Request Owner Team")]
        [ReferenceTarget("team")]
        public InOutArgument<EntityReference> RequestOwnerTeam { get; set; }

        [Output("Request Owner Type")]
        public OutArgument<string> RequestOwnerType { get; set; }

        [Input("Default Request Contact")]
        [Output("Request Contact")]
        [ReferenceTarget("contact")]
        public InOutArgument<EntityReference> RequestContact { get; set; }

        [Input("Default Request Company")]
        [Output("Request Company")]
        [ReferenceTarget("account")]
        public InOutArgument<EntityReference> RequestCompany { get; set; }

        [Input("Default Request Team")]
        [Output("Request Team")]
        [ReferenceTarget("team")]
        public InOutArgument<EntityReference> RequestTeam { get; set; }

        [Input("Default Queue")]
        [Output("Request Queue")]
        [ReferenceTarget("queue")]
        public InOutArgument<EntityReference> RequestQueue { get; set; }

        [Output("Request Title")]
        public OutArgument<string> RequestTitle { get; set; }

        [Output("Create Contact")]
        public OutArgument<bool> CreateContact { get; set; }

        [Output("Contact Not Exist")]
        public OutArgument<bool> ContactNotExist { get; set; }

        [Input("Default Contact Company")]
        [Output("Contact Company")]
        [ReferenceTarget("account")]
        public InOutArgument<EntityReference> ContactCompany { get; set; }

        [Output("Contact Email")]
        public OutArgument<string> ContactEmail { get; set; }

        [Output("Contact Last Name")]
        public OutArgument<string> ContactLastName { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            //Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            string errorMessage = string.Empty;
            EntityReference requestOwner = null;
            string requestOwnerType = string.Empty;
            EntityReference requestContact = null;
            string requestTitle = string.Empty;
            EntityReference requestTeam = null;
            EntityReference requestCompany = null;
            EntityReference requestQueue = null;
            EntityReference contactCompany = null;
            string contactLastName = string.Empty;
            string contactEmail = string.Empty;
            bool createContact = false;
            bool contactNotExist = false;
            string toRecipients = string.Empty;
            string subject = string.Empty;
            string sender = string.Empty;
            int routingType = 0;

            //Guid defaultContactId = this.DefaultContact.Get(executionContext).Id;
            //EntityReference defaultContact = new Entity("contact", defaultContactId).ToEntityReference();
            EntityReference defaultContact = (EntityReference)this.RequestContact.Get(executionContext);
            EntityReference defaultContactCompany = (EntityReference)this.ContactCompany.Get(executionContext);
            EntityReference defaultCompany = (EntityReference)this.RequestCompany.Get(executionContext);
            EntityReference defaultQueue = (EntityReference)this.RequestQueue.Get(executionContext);
            EntityReference defaultTeam = (EntityReference)this.RequestTeam.Get(executionContext);
            EntityReference defaultOwnerTeam = (EntityReference)this.RequestOwnerTeam.Get(executionContext);

            try
            {
                tracingService.Trace("Starting the CGN rules...");
                tracingService.Trace("EmailId: " + context.PrimaryEntityId);

                string ExecuteCGNRoutingRulesResult = ExecuteCGNRoutingRules(service, ref requestOwner, ref requestOwnerType, ref requestContact, ref requestTitle, ref requestTeam, ref requestCompany, ref requestQueue, ref contactCompany, ref contactLastName, ref contactEmail, ref createContact, ref contactNotExist, ref toRecipients, ref sender, ref subject, ref routingType, context.PrimaryEntityId);

                tracingService.Trace("Recipients: " + toRecipients);
                tracingService.Trace("Subject: " + subject);
                tracingService.Trace("Sender: " + sender);
                tracingService.Trace("Routing Type: " + routingType);
                tracingService.Trace("Create Contact: " + createContact);
                tracingService.Trace("Contact Not Exist: " + contactNotExist);
                tracingService.Trace("Contact Last Name: " + contactLastName);
                tracingService.Trace("Contact Email: " + contactEmail);
                if (contactCompany != null)
                {
                    tracingService.Trace("Contact Company Id: " + contactCompany.Id);
                    tracingService.Trace("Contact Company Name: " + contactCompany.Name);
                }
                else
                    tracingService.Trace("Contact Company: NULL");
                if (requestOwner != null)
                {
                    tracingService.Trace("Request Owner Id: " + requestOwner.Id);
                    tracingService.Trace("Request Owner Name: " + requestOwner.Name);
                }
                else
                    tracingService.Trace("Request Owner: NULL");
                tracingService.Trace("Request Owner Type: " + requestOwnerType);
                if (requestContact != null)
                {
                    tracingService.Trace("Request Contact Id: " + requestContact.Id);
                    tracingService.Trace("Request Contact Name: " + requestContact.Name);
                }
                else
                    tracingService.Trace("Request Contact: NULL");
                tracingService.Trace("Request Title: " + requestTitle);
                if (requestTeam != null)
                {
                    tracingService.Trace("Request Team Id: " + requestTeam.Id);
                    tracingService.Trace("Request Team Name: " + requestTeam.Name);
                }
                else
                    tracingService.Trace("Request Team: NULL");
                if (requestCompany != null)
                {
                    tracingService.Trace("Request Company Id: " + requestCompany.Id);
                    tracingService.Trace("Request Company Name: " + requestCompany.Name);
                }
                else
                    tracingService.Trace("Request Company: NULL");
                if (requestQueue != null)
                {
                    tracingService.Trace("Request Queue Id: " + requestQueue.Id);
                    tracingService.Trace("Request Queue Name: " + requestQueue.Name);
                }
                else
                    tracingService.Trace("equest Queue: NULL");

                if (ExecuteCGNRoutingRulesResult.StartsWith("Error"))
                {
                    tracingService.Trace("Error Executing CGN Email Routing Rules: " + ExecuteCGNRoutingRulesResult);                  
                    errorMessage = ExecuteCGNRoutingRulesResult;
                    this.ErrorMessage.Set(executionContext, ExecuteCGNRoutingRulesResult);
                    this.RequestOwnerUser.Set(executionContext, null);
                    this.RequestOwnerTeam.Set(executionContext, null);
                    this.RequestOwnerType.Set(executionContext, string.Empty);
                    this.RequestContact.Set(executionContext, null);
                    this.RequestCompany.Set(executionContext, null);
                    this.RequestTeam.Set(executionContext, null);
                    this.RequestQueue.Set(executionContext, null);
                    this.RequestTitle.Set(executionContext, string.Empty);
                    this.CreateContact.Set(executionContext, false);
                    this.ContactNotExist.Set(executionContext, false);
                    this.ContactCompany.Set(executionContext, null);
                    this.ContactEmail.Set(executionContext, string.Empty);
                    this.ContactLastName.Set(executionContext, string.Empty);
                    return;
                }
                else
                {
                    if (ExecuteCGNRoutingRulesResult == "No Error")
                    {
                        string CheckContactForRequestResult = "Not Checked";
                        tracingService.Trace("Result Executing CGN Email Routing Rules: " + ExecuteCGNRoutingRulesResult);
                        errorMessage = "No Error";
                        this.ErrorMessage.Set(executionContext, "No Error");
                        if (requestOwnerType == "systemuser")
                        {
                            if (requestOwner != null)
                            {
                                this.RequestOwnerUser.Set(executionContext, requestOwner);                                
                                this.RequestOwnerTeam.Set(executionContext, defaultOwnerTeam);
                                this.RequestOwnerType.Set(executionContext, "systemuser");
                            }
                            else
                            {
                                this.RequestOwnerTeam.Set(executionContext, defaultOwnerTeam);
                                this.RequestOwnerUser.Set(executionContext, defaultOwnerTeam);
                                this.RequestOwnerType.Set(executionContext, "team");
                            }
                            
                        }
                        else
                        {
                            if (requestOwner != null)
                            {
                                this.RequestOwnerTeam.Set(executionContext, requestOwner);
                                this.RequestOwnerUser.Set(executionContext, defaultOwnerTeam);
                            }
                            else
                            {
                                this.RequestOwnerTeam.Set(executionContext, defaultOwnerTeam);
                                this.RequestOwnerUser.Set(executionContext, defaultOwnerTeam);
                            }
                            this.RequestOwnerType.Set(executionContext, "team");
                        }

                        if (requestCompany != null)
                        {
                            this.RequestCompany.Set(executionContext, requestCompany);

                            if (requestContact != null)
                            {                                
                                // Check Contact belong to the Request Company
                                CheckContactForRequestResult = CheckContactForRequest(service, requestContact.Id, requestCompany.Id);

                                if (CheckContactForRequestResult == "Yes")
                                {
                                    contactNotExist = false;
                                    this.RequestContact.Set(executionContext, requestContact);
                                }
                                else
                                {
                                    contactNotExist = true;
                                    this.RequestContact.Set(executionContext, defaultContact);
                                }
                            }
                            else
                            {
                                if (createContact)
                                {
                                    this.RequestContact.Set(executionContext, defaultContact);
                                }
                                else
                                {
                                    contactNotExist = true;
                                    this.RequestContact.Set(executionContext, defaultContact);
                                }
                            }
                        }
                        else
                        {
                            this.RequestCompany.Set(executionContext, defaultCompany);
                            this.RequestContact.Set(executionContext, defaultContact);
                        }

                        if (requestTeam != null)
                            this.RequestTeam.Set(executionContext, requestTeam);
                        else
                            this.RequestTeam.Set(executionContext, defaultTeam);

                        if (requestQueue != null)
                            this.RequestQueue.Set(executionContext, requestQueue);
                        else
                            this.RequestQueue.Set(executionContext, defaultQueue);

                        if (contactCompany != null)
                            this.ContactCompany.Set(executionContext, contactCompany);
                        else
                            this.ContactCompany.Set(executionContext, defaultContactCompany);

                        if (contactEmail != string.Empty)
                        {
                            this.ContactEmail.Set(executionContext, contactEmail);
                        }
                        else
                        {
                            this.ContactEmail.Set(executionContext, string.Empty);
                        }

                        if (contactLastName != string.Empty)
                        {
                            this.ContactLastName.Set(executionContext, contactLastName);
                        }
                        else
                        {
                            this.ContactLastName.Set(executionContext, string.Empty);
                        }

                        if (requestTitle != string.Empty)
                        {
                            this.RequestTitle.Set(executionContext, requestTitle);
                        }
                        else
                        {
                            this.RequestTitle.Set(executionContext, string.Empty);
                        }

                        this.CreateContact.Set(executionContext, createContact);

                        this.ContactNotExist.Set(executionContext, contactNotExist);

                        tracingService.Trace("Just Before Return");
                        tracingService.Trace("---------------------------------------------");
                        tracingService.Trace("Recipients: " + toRecipients);
                        tracingService.Trace("Subject: " + subject);
                        tracingService.Trace("Routing Type: " + routingType);
                        tracingService.Trace("Sender: " + sender);
                        tracingService.Trace("Checking Contact belong to the Company: " + CheckContactForRequestResult);
                        tracingService.Trace("Create Contact: " + createContact);
                        tracingService.Trace("Contact Not Exist: " + contactNotExist);
                        tracingService.Trace("Contact Last Name: " + contactLastName);
                        tracingService.Trace("Contact Email: " + contactEmail);
                        if ((EntityReference)this.ContactCompany.Get(executionContext) != null)
                        {
                            tracingService.Trace("Contact Company Id: " + ((EntityReference)this.ContactCompany.Get(executionContext)).Id);
                            tracingService.Trace("Contact Company Name: " + ((EntityReference)this.ContactCompany.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Contact Company: NULL");
                        if ((EntityReference)this.RequestOwnerUser.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Owner User Id: " + ((EntityReference)this.RequestOwnerUser.Get(executionContext)).Id);
                            tracingService.Trace("Request Owner User Name: " + ((EntityReference)this.RequestOwnerUser.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Request Owner User: NULL");
                        if ((EntityReference)this.RequestOwnerTeam.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Owner Team Id: " + ((EntityReference)this.RequestOwnerTeam.Get(executionContext)).Id);
                            tracingService.Trace("Request Owner Team Name: " + ((EntityReference)this.RequestOwnerTeam.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Request Owner Team: NULL");
                        tracingService.Trace("Request Owner Type: " + requestOwnerType);
                        if ((EntityReference)this.RequestContact.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Contact Id: " + ((EntityReference)this.RequestContact.Get(executionContext)).Id);
                            tracingService.Trace("Request Contact Name: " + ((EntityReference)this.RequestContact.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Request Contact: NULL");
                        tracingService.Trace("Request Title: " + requestTitle);
                        if ((EntityReference)this.RequestTeam.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Team Id: " + ((EntityReference)this.RequestTeam.Get(executionContext)).Id);
                            tracingService.Trace("Request Team Name: " + ((EntityReference)this.RequestTeam.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Request Team: NULL");
                        if ((EntityReference)this.RequestCompany.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Company Id: " + ((EntityReference)this.RequestCompany.Get(executionContext)).Id);
                            tracingService.Trace("Request Company Name: " + ((EntityReference)this.RequestCompany.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("Request Company: NULL");
                        if ((EntityReference)this.RequestQueue.Get(executionContext) != null)
                        {
                            tracingService.Trace("Request Queue Id: " + ((EntityReference)this.RequestQueue.Get(executionContext)).Id);
                            tracingService.Trace("Request Queue Name: " + ((EntityReference)this.RequestQueue.Get(executionContext)).Name);
                        }
                        else
                            tracingService.Trace("equest Queue: NULL");

                        return;
                    }
                    else if (ExecuteCGNRoutingRulesResult == "No Routing Found")
                    {
                        tracingService.Trace("Result Executing CGN Email Routing Rules: " + ExecuteCGNRoutingRulesResult);
                        errorMessage =  "No Routing Found";
                        this.ErrorMessage.Set(executionContext, "No Routing Found");
                        this.RequestOwnerUser.Set(executionContext, null);
                        this.RequestOwnerTeam.Set(executionContext, null);
                        this.RequestOwnerType.Set(executionContext, string.Empty);
                        this.RequestContact.Set(executionContext, null);
                        this.RequestCompany.Set(executionContext, null);
                        this.RequestTeam.Set(executionContext, null);
                        this.RequestQueue.Set(executionContext, null);
                        this.RequestTitle.Set(executionContext, string.Empty);
                        this.CreateContact.Set(executionContext, false);
                        this.ContactCompany.Set(executionContext, null);
                        this.ContactEmail.Set(executionContext, string.Empty);
                        this.ContactLastName.Set(executionContext, string.Empty);
                        return;
                    }
                    else
                    {
                        tracingService.Trace("Unknow Executing CGN Email Routing Rules: " + ExecuteCGNRoutingRulesResult);
                        errorMessage = "Error - Unknown";
                        this.ErrorMessage.Set(executionContext, "Error - Unknown");
                        this.RequestOwnerUser.Set(executionContext, null);
                        this.RequestOwnerTeam.Set(executionContext, null);
                        this.RequestOwnerType.Set(executionContext, string.Empty);
                        this.RequestContact.Set(executionContext, null);
                        this.RequestCompany.Set(executionContext, null);
                        this.RequestTeam.Set(executionContext, null);
                        this.RequestQueue.Set(executionContext, null);
                        this.RequestTitle.Set(executionContext, string.Empty);
                        this.CreateContact.Set(executionContext, false);
                        this.ContactCompany.Set(executionContext, null);
                        this.ContactEmail.Set(executionContext, string.Empty);
                        this.ContactLastName.Set(executionContext, string.Empty);
                        return;
                    }
                }          
            }
            catch (Exception ex)
            {
                tracingService.Trace(ex.Message);
                errorMessage = "Error in CGNEmailRouting: " + ex.Message;
                this.ErrorMessage.Set(executionContext, ex.Message);
            }
            finally
            {
                if (errorMessage.StartsWith("Error"))
                {
                    throw new InvalidPluginExecutionException("An error occurred in the CGNEmailRouting: " + errorMessage);
                }
            }
        }

        public EntityReference GetContactForRequest(IOrganizationService service, Guid accountId)
        {
            EntityReference contactEntity = null;

            try
            {
                if (accountId != Guid.Empty)
                {
                    QueryExpression queryContact = new QueryExpression
                    {
                        EntityName = "contact",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression
                        {
                            Conditions =
                            {
                                new ConditionExpression
                                {
                                    AttributeName = "parentcustomerid",
                                    Operator = ConditionOperator.Equal,
                                    Values = { accountId }
                                }
                            }
                        },
                        Orders =
                        {
                            new OrderExpression
                            {
                                AttributeName = "fullname",
                                OrderType = OrderType.Ascending
                            }
                        }
                    };

                    EntityCollection ContactCollection = service.RetrieveMultiple(queryContact);

                    if (ContactCollection.Entities.Count > 0)
                    {
                        Entity oContact = ContactCollection.Entities[0];
                        contactEntity = oContact.ToEntityReference();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return contactEntity;
        }

        public string CheckContactForRequest(IOrganizationService service, Guid contactId, Guid accountId)
        {
            string result = "No";

            try
            {
                if (accountId != Guid.Empty && contactId != Guid.Empty)
                {
                    Entity oContact = new Entity("contact");
                    ColumnSet attributes = new ColumnSet("parentcustomerid");

                    oContact = service.Retrieve("contact", contactId, attributes);

                    if (oContact != null)
                    {
                        if(oContact.Contains("parentcustomerid"))
                        {
                            if(((EntityReference)oContact["parentcustomerid"]).Id == accountId)
                            {
                                return "Yes";
                            }
                            else
                            {
                                return "No";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error checking contact: " + ex.Message;
            }

            return result;
        }

        public string ExecuteCGNRoutingRules(IOrganizationService service, ref EntityReference requestOwner, ref string requestOwnerType, ref EntityReference requestContact, ref string requestTitle, ref EntityReference requestTeam, ref EntityReference requestCompany, ref EntityReference requestQueue, ref EntityReference contactCompany, ref string contactLastName, ref string contactEmail, ref bool createContact, ref bool contactNotExist, ref string toRecipients, ref string sender, ref string subject, ref int routingType, Guid emailId)
        {
            string result = string.Empty;
            toRecipients = string.Empty;
            subject = string.Empty;
            sender = string.Empty;
            routingType = 0;
            Guid queueId = Guid.Empty;
            Guid companyId = Guid.Empty;
            Guid primaryISR = Guid.Empty;
            Guid companyRequestTeamId = Guid.Empty;
            requestOwner = null;
            requestOwnerType = string.Empty;
            requestContact = null;
            requestTitle = string.Empty;
            requestTeam = null;
            requestCompany = null;
            requestQueue = null;
            contactCompany = null;
            contactLastName = string.Empty;
            contactEmail = string.Empty;
            bool routingFound = false;
            createContact = false;
            contactNotExist = false;

            try
            {
                if (emailId != Guid.Empty)
                {
                    Entity oEmail = new Entity("email");
                    ColumnSet attributes = new ColumnSet("sender", "torecipients", "subject");

                    oEmail = service.Retrieve("email", emailId, attributes);

                    if (oEmail != null)
                    {
                        if (oEmail.Contains("torecipients"))
                        {
                            toRecipients = (string)oEmail["torecipients"];

                            string[] recipients = toRecipients.Split(';');

                            if (oEmail.Contains("sender"))
                            {
                                sender = (string)oEmail["sender"];
                            }

                            if (oEmail.Contains("subject"))
                            {
                                subject = (string)oEmail["subject"];
                                requestTitle = subject;
                            }

                            if (recipients.Length > 0)
                            {
                                foreach(string emailAddress in recipients)
                                {
                                    #region Check the email can be found in the CGN Email Routing table

                                    try
                                    {
                                        QueryExpression queryCGNRouting = new QueryExpression
                                        {
                                            EntityName = "new_cgnemailrouting",
                                            ColumnSet = new ColumnSet("new_cgnemailroutingid", "new_account", "new_queue", "new_routingtype"),
                                            Criteria = new FilterExpression
                                            {
                                                Conditions =
                                                {
                                                    new ConditionExpression
                                                    {
                                                        AttributeName = "new_name",
                                                        Operator = ConditionOperator.Equal,
                                                        Values = { emailAddress }
                                                    }
                                                }
                                            },
                                            Orders =
                                            {
                                                new OrderExpression
                                                {
                                                    AttributeName = "new_name",
                                                    OrderType = OrderType.Ascending
                                                }
                                            }
                                        };

                                        EntityCollection CGNRoutingCollection = service.RetrieveMultiple(queryCGNRouting);

                                        if (CGNRoutingCollection.Entities.Count > 0)
                                        {
                                            Entity oCGNRouting = CGNRoutingCollection.Entities[0];

                                            routingType = 0;
                                            queueId = Guid.Empty;
                                            companyId = Guid.Empty;

                                            //1 - Direct to Company
                                            //2 - Direct to Queue

                                            if (oCGNRouting.Contains("new_routingtype"))
                                            {
                                                routingType = ((OptionSetValue)oCGNRouting["new_routingtype"]).Value;
                                            }

                                            if (oCGNRouting.Contains("new_queue"))
                                            {
                                                queueId = ((EntityReference)oCGNRouting["new_queue"]).Id;
                                                requestQueue = (EntityReference)oCGNRouting["new_queue"];
                                            }

                                            if (oCGNRouting.Contains("new_account"))
                                            {
                                                companyId = ((EntityReference)oCGNRouting["new_account"]).Id;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        result = "Error in ExecuteCGNRoutingRules - Reading CGN Email Routing table: " + ex.ToString();
                                        return result;
                                    }

                                    #endregion

                                    // Check Valid Routing
                                    if (routingType == 1 && companyId != Guid.Empty)
                                    {
                                        routingFound = true;
                                        break;
                                    }
                                    else if (routingType == 2 && queueId != Guid.Empty)
                                    {
                                        routingFound = true;
                                        break;
                                    }
                                }

                                if (routingFound)
                                {
                                    #region Get the Contact (Sender)

                                    requestContact = null;
                                    string[] senderDomain = null;
                                    createContact = false;

                                    if (sender != string.Empty)
                                    {
                                        if (sender.Contains("@"))
                                        {
                                            senderDomain = sender.Split('@');
                                        }
                                        else
                                        {
                                            senderDomain[0] = sender;
                                        }

                                        #region Check the sender email can be found in the Contact table

                                        try
                                        {
                                            QueryExpression queryContact = new QueryExpression
                                            {
                                                EntityName = "contact",
                                                ColumnSet = new ColumnSet(true),
                                                Criteria = new FilterExpression
                                                {
                                                    Conditions =
                                                    {
                                                        new ConditionExpression
                                                        {
                                                            AttributeName = "emailaddress1",
                                                            Operator = ConditionOperator.Equal,
                                                            Values = { sender }
                                                        }
                                                    }
                                                },
                                                Orders =
                                                {
                                                    new OrderExpression
                                                    {
                                                        AttributeName = "fullname",
                                                        OrderType = OrderType.Ascending
                                                    }
                                                }
                                            };

                                            EntityCollection ContactCollection = service.RetrieveMultiple(queryContact);

                                            if (ContactCollection.Entities.Count > 0)
                                            {
                                                Entity oContact = ContactCollection.Entities[0];

                                                if (oContact.Contains("parentcustomerid") && companyId == Guid.Empty)
                                                {
                                                    requestCompany = (EntityReference)oContact["parentcustomerid"];
                                                }

                                                requestContact = oContact.ToEntityReference();
                                                createContact = false;
                                                contactNotExist = false;
                                            }
                                            else
                                            {
                                                #region Sender not Found - Query the Companies for same domain                                                

                                                try
                                                {
                                                    if (senderDomain.Length > 1)
                                                    {
                                                        QueryExpression queryDomain = new QueryExpression
                                                        {
                                                            EntityName = "new_cgndomainmatching",
                                                            ColumnSet = new ColumnSet("new_cgndomainmatchingid", "new_account"),
                                                            Criteria = new FilterExpression
                                                            {
                                                                Conditions =
                                                                    {
                                                                        new ConditionExpression
                                                                        {
                                                                            AttributeName = "new_name",
                                                                            Operator = ConditionOperator.Equal,
                                                                            Values = { senderDomain[1] }
                                                                        }
                                                                    }
                                                            }
                                                        };

                                                        EntityCollection DomainCollection = service.RetrieveMultiple(queryDomain);

                                                        if (DomainCollection.Entities.Count > 0)
                                                        {
                                                            Entity oDomain = DomainCollection.Entities[0];

                                                            if (oDomain.Contains("new_account"))
                                                            {
                                                                requestCompany = (EntityReference)oDomain["new_account"];
                                                                contactCompany = (EntityReference)oDomain["new_account"];
                                                                contactLastName = senderDomain[0];
                                                                contactEmail = sender;
                                                                createContact = true;
                                                            }
                                                            else
                                                            {
                                                                createContact = false;
                                                                contactNotExist = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            createContact = false;
                                                            contactNotExist = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        createContact = false;
                                                        contactNotExist = true;
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    result = "Error in ExecuteCGNRoutingRules - Reading Company Domain: " + sender + " - " + senderDomain[0] + " - " + senderDomain[1] + " - " + ex.ToString();
                                                    return result;
                                                }

                                                #endregion
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            result = "Error in ExecuteCGNRoutingRules - Reading Contact (sender) Information: " + ex.ToString();
                                            return result;
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        result = "No Sender";
                                    }

                                    #endregion

                                    if (routingType == 1 && companyId != Guid.Empty)
                                    {
                                        #region Check the Company Information

                                        try
                                        {
                                            Entity oCompany = service.Retrieve("account", companyId, new ColumnSet(true));

                                            if (oCompany != null)
                                            {
                                                primaryISR = Guid.Empty;
                                                companyRequestTeamId = Guid.Empty;

                                                if (oCompany.Contains("new_isrid"))
                                                {
                                                    primaryISR = ((EntityReference)oCompany["new_isrid"]).Id;
                                                    requestOwner = (EntityReference)oCompany["new_isrid"];
                                                    requestOwnerType = "systemuser";
                                                }

                                                if (oCompany.Contains("new_companyrequestteamid"))
                                                {
                                                    companyRequestTeamId = ((EntityReference)oCompany["new_companyrequestteamid"]).Id;
                                                    requestTeam = (EntityReference)oCompany["new_companyrequestteamid"];
                                                    if (requestOwner == null)
                                                    {
                                                        requestOwner = (EntityReference)oCompany["new_companyrequestteamid"];
                                                        requestOwnerType = "team";
                                                    }
                                                }

                                                requestCompany = oCompany.ToEntityReference();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            result = "Error in ExecuteCGNRoutingRules - Reading Company Information: " + ex.ToString();
                                            return result;
                                        }

                                        #endregion

                                        if (primaryISR != Guid.Empty)
                                        {
                                            #region Get the User Queue
                                            try
                                            {
                                                Entity oUser = new Entity("systemuser");
                                                ColumnSet userAttributes = new ColumnSet(true);

                                                oUser = service.Retrieve("systemuser", primaryISR, userAttributes);

                                                if (oUser != null)
                                                {
                                                    if (oUser.Contains("queueid"))
                                                    {
                                                        requestQueue = (EntityReference)oUser["queueid"];
                                                        //return "Error: requestQueue: " + requestQueue.Name + " - " + requestQueue.Id;
                                                    }
                                                    else
                                                    {
                                                        requestQueue = null;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Error in ExecuteCGNRoutingRules - Reading User Queue Information: " + ex.ToString();
                                                return result;
                                            }

                                            #endregion
                                        }
                                        else if (companyRequestTeamId != Guid.Empty)
                                        {
                                            #region Get the Team Queue

                                            try
                                            {
                                                Entity oTeam = new Entity("team");
                                                ColumnSet teamAttributes = new ColumnSet(true);

                                                oTeam = service.Retrieve("team", companyRequestTeamId, teamAttributes);

                                                if (oTeam != null)
                                                {
                                                    if (oTeam.Contains("queueid"))
                                                    {
                                                        requestQueue = (EntityReference)oTeam["queueid"];
                                                    }
                                                    else
                                                    {
                                                        requestQueue = null;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Error in ExecuteCGNRoutingRules - Reading Team Queue Information: " + ex.ToString();
                                                return result;
                                            }

                                            #endregion
                                        }
                                        else
                                        {
                                            requestOwnerType = "none";
                                            requestQueue = null;
                                        }

                                        result = "No Error";
                                    }
                                    else if (routingType == 1 && companyId == Guid.Empty)
                                    {
                                        #region Company not provided - Query the Companies for same domain                                                

                                        if (requestCompany == null)
                                        {
                                            #region Checking CGN Domain Matching

                                            try
                                            {
                                                if (sender.Contains("@"))
                                                {
                                                    senderDomain = sender.Split('@');

                                                    if (senderDomain.Length > 1)
                                                    {
                                                        QueryExpression queryDomain = new QueryExpression
                                                        {
                                                            EntityName = "new_cgndomainmatching",
                                                            ColumnSet = new ColumnSet("new_cgndomainmatchingid", "new_account"),
                                                            Criteria = new FilterExpression
                                                            {
                                                                Conditions =
                                                                    {
                                                                        new ConditionExpression
                                                                        {
                                                                            AttributeName = "new_name",
                                                                            Operator = ConditionOperator.Equal,
                                                                            Values = { senderDomain[1] }
                                                                        }
                                                                    }
                                                            }
                                                        };

                                                        EntityCollection DomainCollection = service.RetrieveMultiple(queryDomain);

                                                        if (DomainCollection.Entities.Count > 0)
                                                        {
                                                            Entity oDomain = DomainCollection.Entities[0];

                                                            if (oDomain.Contains("new_account"))
                                                            {
                                                                requestCompany = (EntityReference)oDomain["new_account"];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Error in ExecuteCGNRoutingRules 2 - Reading Company Domain: " + sender + " - " + senderDomain[0] + " - " + senderDomain[1] + " - " + ex.ToString();
                                                return result;
                                            }

                                            #endregion
                                        }
                                        else
                                        {
                                            #region Check the Company Information

                                            try
                                            {
                                                Entity oCompany = service.Retrieve("account", requestCompany.Id, new ColumnSet(true));

                                                if (oCompany != null)
                                                {
                                                    primaryISR = Guid.Empty;
                                                    companyRequestTeamId = Guid.Empty;

                                                    if (oCompany.Contains("new_isrid"))
                                                    {
                                                        primaryISR = ((EntityReference)oCompany["new_isrid"]).Id;
                                                        requestOwner = (EntityReference)oCompany["new_isrid"];
                                                        requestOwnerType = "systemuser";
                                                    }

                                                    if (oCompany.Contains("new_companyrequestteamid"))
                                                    {
                                                        companyRequestTeamId = ((EntityReference)oCompany["new_companyrequestteamid"]).Id;
                                                        requestTeam = (EntityReference)oCompany["new_companyrequestteamid"];
                                                        if (requestOwner == null)
                                                        {
                                                            requestOwner = (EntityReference)oCompany["new_companyrequestteamid"];
                                                            requestOwnerType = "team";
                                                        }
                                                    }

                                                    requestCompany = oCompany.ToEntityReference();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Error in ExecuteCGNRoutingRules - Reading Company Information: " + ex.ToString();
                                                return result;
                                            }

                                            #endregion

                                            if (primaryISR != Guid.Empty)
                                            {
                                                #region Get the User Queue
                                                try
                                                {
                                                    Entity oUser = new Entity("systemuser");
                                                    ColumnSet userAttributes = new ColumnSet("queueid");

                                                    oUser = service.Retrieve("systemuser", primaryISR, userAttributes);

                                                    if (oUser != null)
                                                    {
                                                        if (oUser.Contains("queueid"))
                                                        {
                                                            requestQueue = (EntityReference)oUser["queueid"];
                                                        }
                                                        else
                                                        {
                                                            requestQueue = null;
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    result = "Error in ExecuteCGNRoutingRules - Reading User Queue Information: " + ex.ToString();
                                                    return result;
                                                }

                                                #endregion
                                            }
                                            else if (companyRequestTeamId != Guid.Empty)
                                            {
                                                #region Get the Team Queue

                                                try
                                                {
                                                    Entity oTeam = new Entity("team");
                                                    ColumnSet teamAttributes = new ColumnSet("queueid");

                                                    oTeam = service.Retrieve("team", companyRequestTeamId, teamAttributes);

                                                    if (oTeam != null)
                                                    {
                                                        if (oTeam.Contains("queueid"))
                                                        {
                                                            requestQueue = (EntityReference)oTeam["queueid"];
                                                        }
                                                        else
                                                        {
                                                            requestQueue = null;
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    result = "Error in ExecuteCGNRoutingRules - Reading Team Queue Information: " + ex.ToString();
                                                    return result;
                                                }

                                                #endregion
                                            }
                                            else
                                            {
                                                requestOwnerType = "none";
                                                requestQueue = null;
                                            }

                                            result = "No Error";
                                        }

                                        if (requestCompany == null)
                                        {
                                            #region Convert to "Direct to Queue"

                                            if (queueId != Guid.Empty)
                                            {
                                                #region Get the Queue Owner

                                                try
                                                {
                                                    QueryExpression queryTeam = new QueryExpression
                                                    {
                                                        EntityName = "team",
                                                        ColumnSet = new ColumnSet("teamid"),
                                                        Criteria = new FilterExpression
                                                        {
                                                            Conditions =
                                                            {
                                                                new ConditionExpression
                                                                {
                                                                    AttributeName = "queueid",
                                                                    Operator = ConditionOperator.Equal,
                                                                    Values = { queueId }
                                                                }
                                                            }
                                                        }
                                                    };

                                                    EntityCollection TeamCollection = service.RetrieveMultiple(queryTeam);

                                                    if (TeamCollection != null)
                                                    {
                                                        if (TeamCollection.Entities.Count > 0)
                                                        {
                                                            Entity oTeam = TeamCollection.Entities[0];
                                                            requestOwner = oTeam.ToEntityReference();
                                                            requestOwnerType = "team";
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    result = "Error in ExecuteCGNRoutingRules 2 - Reading Queue Owner Information: " + ex.ToString();
                                                    return result;
                                                }

                                                if (requestOwner == null)
                                                {
                                                    try
                                                    {
                                                        QueryExpression queryUser = new QueryExpression
                                                        {
                                                            EntityName = "systemuser",
                                                            ColumnSet = new ColumnSet("systemuserid"),
                                                            Criteria = new FilterExpression
                                                            {
                                                                Conditions =
                                                                {
                                                                    new ConditionExpression
                                                                    {
                                                                        AttributeName = "queueid",
                                                                        Operator = ConditionOperator.Equal,
                                                                        Values = { queueId }
                                                                    }
                                                                }
                                                            }
                                                        };

                                                        EntityCollection UserCollection = service.RetrieveMultiple(queryUser);

                                                        if (UserCollection != null)
                                                        {
                                                            if (UserCollection.Entities.Count > 0)
                                                            {
                                                                Entity oUser = UserCollection.Entities[0];
                                                                requestOwner = oUser.ToEntityReference();
                                                                requestOwnerType = "systemuser";
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        result = "Error in ExecuteCGNRoutingRules 2 - Reading Queue User Information: " + ex.ToString();
                                                        return result;
                                                    }
                                                }

                                                #endregion

                                                result = "No Error";
                                            }

                                            #endregion
                                        }

                                        #endregion
                                    }
                                    else if (routingType == 2 && queueId != Guid.Empty)
                                    {
                                        #region Get the Queue Owner

                                        try
                                        {
                                            QueryExpression queryTeam = new QueryExpression
                                            {
                                                EntityName = "team",
                                                ColumnSet = new ColumnSet(true),
                                                Criteria = new FilterExpression
                                                {
                                                    Conditions =
                                                    {
                                                        new ConditionExpression
                                                        {
                                                            AttributeName = "queueid",
                                                            Operator = ConditionOperator.Equal,
                                                            Values = { queueId }
                                                        }
                                                    }
                                                }
                                            };

                                            EntityCollection TeamCollection = service.RetrieveMultiple(queryTeam);

                                            if (TeamCollection != null)
                                            {
                                                if (TeamCollection.Entities.Count > 0)
                                                {
                                                    Entity oTeam = TeamCollection.Entities[0];
                                                    requestOwner = oTeam.ToEntityReference();
                                                    requestOwnerType = "team";
                                                    requestTeam = oTeam.ToEntityReference();
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            result = "Error in ExecuteCGNRoutingRules - Reading Queue Owner Information: " + ex.ToString();
                                            return result;
                                        }

                                        if (requestOwner == null)
                                        {
                                            try
                                            {
                                                QueryExpression queryUser = new QueryExpression
                                                {
                                                    EntityName = "systemuser",
                                                    ColumnSet = new ColumnSet(true),
                                                    Criteria = new FilterExpression
                                                    {
                                                        Conditions =
                                                        {
                                                            new ConditionExpression
                                                            {
                                                                AttributeName = "queueid",
                                                                Operator = ConditionOperator.Equal,
                                                                Values = { queueId }
                                                            }
                                                        }
                                                    }
                                                };

                                                EntityCollection UserCollection = service.RetrieveMultiple(queryUser);

                                                if (UserCollection != null)
                                                {
                                                    if (UserCollection.Entities.Count > 0)
                                                    {
                                                        Entity oUser = UserCollection.Entities[0];
                                                        requestOwner = oUser.ToEntityReference();
                                                        requestOwnerType = "systemuser";
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Error in ExecuteCGNRoutingRules - Reading Queue User Information: " + ex.ToString();
                                                return result;
                                            }
                                        }

                                        #endregion

                                        result = "No Error";
                                    }
                                    else
                                    {
                                        result = "No Routing Found";
                                        return result;
                                    }
                                }
                                else
                                {
                                    result = "No Routing Found";
                                    return result;
                                }
                            }
                            else
                            {
                                result = "Recipient was not provided";
                            }
                        }
                        else
                        {
                            result = "Recipient was not provided";
                        }
                    }
                    else
                    {
                        result = "Recipient was not provided";
                    }
                }
                else
                {
                    result = "No EmailId";
                }
            }
            catch (Exception ex)
            {
                result = "ExecuteCGNRoutingRules: " + ex.ToString();
            }

            return result;
        }
    }
}
